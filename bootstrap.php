<?php
declare(strict_types=1);

define('APP_ROOT', __DIR__ . '/');
define('APP_CONFIG_DIR', APP_ROOT . 'config/');

require __DIR__ . '/vendor/autoload.php';
