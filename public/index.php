<?php
declare(strict_types=1);

use App\App;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\ErrorHandler\ErrorHandler;
use Symfony\Component\HttpFoundation\Request;

require_once (__DIR__.'/../bootstrap.php');

if(getenv('APP_ENV') === App::ENV_DEV) {
    Debug::enable();
} else {
    ErrorHandler::register();
}

// initialize the DI container
$containerBuilder = new ContainerBuilder();
$loader = new PhpFileLoader($containerBuilder, new FileLocator(APP_CONFIG_DIR));
$loader->load('di.php');
$containerBuilder->compile();

// run the app
$containerBuilder->get(App::class)->run(
    $containerBuilder,
    Request::createFromGlobals()
);
