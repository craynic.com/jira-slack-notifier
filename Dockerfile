FROM registry.gitlab.com/craynic.com/docker/lap:1

# create webroot
RUN mkdir -p /var/www/notifier/htdocs
WORKDIR /var/www/notifier/htdocs

# copy the App
COPY config ./config
COPY public ./public
COPY src ./src
COPY .htaccess .
COPY bootstrap.php .
COPY composer.json .
COPY composer.lock .

# copy other files
COPY docker/files/ /

# make sure the web config script is executable
RUN chmod +x /usr/local/sbin/configure-web.d/200-notifier.sh

ENV MYSQL_HOST="localhost" \
    MYSQL_PORT="3306" \
    MYSQL_DATABASE="notifier" \
    MYSQL_USER="notifier" \
    MYSQL_PASSWORD=""

# install production composer
RUN composer install --no-dev
