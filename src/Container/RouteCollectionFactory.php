<?php
declare(strict_types=1);

namespace App\Container;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\PhpFileLoader;
use Symfony\Component\Routing\RouteCollection;

final class RouteCollectionFactory
{
    public function __invoke(): RouteCollection
    {
        $fileLocator = new FileLocator(APP_CONFIG_DIR);
        $loader = new PhpFileLoader($fileLocator);

        return $loader->load('routes.php');
    }
}