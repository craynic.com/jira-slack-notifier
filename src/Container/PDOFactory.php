<?php
declare(strict_types=1);

namespace App\Container;

use PDO;

final class PDOFactory
{
    public function __invoke(): PDO
    {
        $dsn = sprintf(
            'mysql:host=%1$s;port=%2$d;dbname=%3$s',
            getenv('MYSQL_HOST'),
            (int) getenv('MYSQL_PORT'),
            getenv('MYSQL_DATABASE')
        );

        $username = getenv('MYSQL_USER');
        $password = getenv('MYSQL_PASSWORD');

        $options = [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];

        return new PDO($dsn, $username, $password, $options);
    }
}