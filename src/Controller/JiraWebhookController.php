<?php
declare(strict_types=1);

namespace App\Controller;

use App\IncomingPayload\Payload;
use App\IncomingPayload\Repository;
use DateTimeImmutable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class JiraWebhookController
{
    private Repository $repository;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function post(Request $request): Response
    {
        $payload = Payload::createNew(
            $request->getContent(),
            new DateTimeImmutable()
        );

        $this->repository->create($payload);

        return new Response();
    }
}