<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

final class IndexController
{
    public function get(): Response
    {
        return new Response('OK');
    }
}