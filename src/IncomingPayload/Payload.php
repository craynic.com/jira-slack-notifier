<?php
declare(strict_types=1);

namespace App\IncomingPayload;

use DateTimeImmutable;
use Ramsey\Uuid\UuidInterface;

final class Payload
{
    private ?UuidInterface $id;
    private string $contents;
    private DateTimeImmutable $dateTime;

    public static function createNew(string $contents, DateTimeImmutable $dateTime): self
    {
        return new Payload(null, $contents, $dateTime);
    }

    public static function createExisting(UuidInterface $id, string $contents, DateTimeImmutable $dateTime) : self
    {
        return new Payload($id, $contents, $dateTime);
    }

    private function __construct(?UuidInterface $id, string $contents, DateTimeImmutable $dateTime)
    {
        $this->id = $id;
        $this->contents = $contents;
        $this->dateTime = $dateTime;
    }

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function getContents(): string
    {
        return $this->contents;
    }

    public function getDateTime(): DateTimeImmutable
    {
        return $this->dateTime;
    }
}