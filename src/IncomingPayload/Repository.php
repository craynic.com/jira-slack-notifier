<?php
declare(strict_types=1);

namespace App\IncomingPayload;

use PDO;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class Repository
{
    private PDO $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function create(Payload $payload): UuidInterface
    {
        $statement = $this->pdo->prepare(
            'INSERT INTO `incoming_payload` (`id`, `contents`, `datetime`) VALUES (?, ?, ?)'
        );

        $newUuid = Uuid::uuid4();
        $statement->execute([
            $newUuid->toString(),
            $payload->getContents(),
            $payload->getDateTime()->format('Y-m-d H:i:s')
        ]);

        return $newUuid;
    }

    public function remove(Payload $payload): void
    {
        $statement = $this->pdo->prepare(
            'DELETE FROM `incoming_payload` WHERE `id`=?',
        );

        $statement->execute([
            $payload->getId()->toString(),
        ]);
    }
}