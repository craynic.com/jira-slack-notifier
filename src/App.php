<?php
declare(strict_types=1);

namespace App;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

final class App
{
    public const ENV_DEV = 'dev';

    private RouteCollection $routes;

    public function __construct(RouteCollection $routes)
    {
        $this->routes = $routes;
    }

    public function run(ContainerInterface $container, Request $request): void
    {
        // create request
        $requestContext = new RequestContext();
        $requestContext->fromRequest($request);

        // create URL matcher
        $matcher = new UrlMatcher($this->routes, $requestContext);

        // event dispatcher
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));

        // controller & argument resolvers
        $controllerResolver = new ContainerControllerResolver($container);
        $argumentResolver = new ArgumentResolver();

        // new kernel
        $kernel = new HttpKernel($dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);

        // handle request
        try {
            $response = $kernel->handle($request);
        } catch (NotFoundHttpException $exception) {
            $this->handleNotFound();
            return;
        }

        $response->send();
        $kernel->terminate($request, $response);
    }

    private function handleNotFound(): void
    {
        header('HTTP/1.1 404 Not Found');
    }
}