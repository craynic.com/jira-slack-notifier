CREATE TABLE IF NOT EXISTS `incoming_payload`
(
    `id`       CHAR(36) NOT NULL PRIMARY KEY,
    `contents` LONGTEXT NOT NULL,
    `datetime` DATETIME NOT NULL,
    INDEX `datetime` (`datetime` ASC)
);
