#!/usr/bin/env bash

set -Eeuo pipefail

PHP_VERSION="7.4"
DOC_ROOT="/var/www"
APACHE_USER="www-data"
APACHE_GROUP="www-data"
APACHE_SITE_NAME="001-notifier"
HOME_DIR="${DOC_ROOT}/notifier"

# Apache configuration
realm-admin "apache:generate-vhost-conf" \
  -a "*" \
  --php-version "${PHP_VERSION}" \
  --home-dir "${HOME_DIR}" \
  -- \
  "notifier" \
  >"/etc/apache2/sites-available/${APACHE_SITE_NAME}.conf"

# enable default sites
a2ensite "${APACHE_SITE_NAME}" >/dev/null

# PHP configuration
realm-admin "php:generate-pool-conf" \
  -m 128 \
  -t 30 \
  --home-dir "${HOME_DIR}" \
  -- \
  "notifier" \
  "${APACHE_USER}" "${APACHE_GROUP}" "${PHP_VERSION}" \
  >"/etc/php/${PHP_VERSION}/fpm/pool.d/notifier.conf"