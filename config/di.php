<?php
/** @noinspection PhpUnusedParameterInspection */
declare(strict_types=1);

use App\Container\PDOFactory;
use App\Container\RouteCollectionFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\Routing\RouteCollection;

use function Symfony\Component\DependencyInjection\Loader\Configurator\ref;

return function (ContainerConfigurator $configurator, ContainerBuilder $containerBuilder): void {
    $services = $configurator->services()
        ->defaults()
        ->autowire();

    $services->load('App\\', APP_ROOT . 'src/**/*');

    // $services->set(JiraWebhookController::class);
    $services->set(RouteCollection::class)->factory(ref(RouteCollectionFactory::class));
    $services->set(PDO::class, PDO::class)->factory(ref(PDOFactory::class));
};
