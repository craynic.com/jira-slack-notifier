<?php
declare(strict_types=1);

use App\Controller\IndexController;
use App\Controller\JiraWebhookController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->add('index', '/')
        ->controller([IndexController::class, 'get'])
        ->methods([Request::METHOD_GET]);

    $routes->add('jira', '/jira')
        ->controller([JiraWebhookController::class, 'post'])
        ->methods([Request::METHOD_POST]);
};
