<?php
declare(strict_types=1);

namespace PHPSTORM_META {

    use Symfony\Component\DependencyInjection\ContainerInterface;

    override(
        ContainerInterface::get(0),
        type(0)
    );
}
